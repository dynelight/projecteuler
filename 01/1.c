#include <math.h>
#include <stdio.h>

int main(){
  int acc = 0;
  int i;
  for(i = 1; i < 1000; i++){
    if(((i % 5) == 0 )||((i % 3) == 0)){
      acc += i;
    }

  }
  printf("%d\n", acc);
  return 0;
}
