#include <math.h>
#include <stdio.h>

int main(){
  int acc = 0;
  int fib = 0;
  int i = 0;
  int prevFib = 0;
  int prevPrevFib = 0;

  do {
    if(i == 0){
      fib = 0;
    } else
    if(i == 1){
      fib = 1;
      prevFib = 1;
      prevPrevFib = 0;
    } else {
     fib = prevFib + prevPrevFib;
     if(fib % 2 == 0){
      acc += fib;
     }
     prevPrevFib = prevFib;
     prevFib = fib;

    }
    i++;

  }
  while(fib < 4000000);
  printf("%d\n", acc);
  return 0;
}
