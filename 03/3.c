#include <math.h>
#include <stdio.h>

int is_prime(long);

int main(){
  long i;
  long max_prime = 0;
  for(i = 1; i <= 600851475143; i++){

    if (is_prime(i) == 1){
      if(600851475143 % i == 0){
        max_prime = i;
        printf("%ld\n", max_prime);
      }
    }
  }
  printf("%ld\n", max_prime);
  return 0;
}

int is_prime(long num){
  long j;
  int is_prime = 1;
  for(j = 2; j < sqrt(num); j++){
    if(num % j == 0){
      is_prime = 0;
    }
  }
  return is_prime;

}
