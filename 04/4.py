def is_palindrome(product):
    str_product =  str(product)
    i = 0
    j = len(str_product) - 1
    palindrome = True
    
    while i < j and palindrome:
        if str_product[i] == str_product[j]:
            i = i + 1
            j = j - 1
        else: 
            palindrome = False
    return palindrome
    
max_palindrome = -1
max_i = -1
max_j = -1
for i in range(100, 1000):
    for j in range(i, 1000):
        if is_palindrome(i*j):
            if i*j > max_palindrome:
                max_palindrome = i*j
                max_i = i
                max_j = j
                
print max_i, max_j, max_palindrome
