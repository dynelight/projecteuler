list_pen = []
limit = 10

def generate_pentagonals(limit):
    global list_pen
    for i in range(0, limit*(3*limit-1/2) + 1):
        list_pen.append(False)
    for i in range(1, limit+1):
        list_pen[i*(3*i-1)/2] = True


generate_pentagonals(limit)

print "After generation... "

for i in range(1, limit):
    for j in range(i, limit):
 #       if list_pen[i+j] and list_pen[i-j]:
        if list_pen[i+j]:
            print i, j, i-j
