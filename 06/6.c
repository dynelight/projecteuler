#include <math.h>
#include <stdio.h>

int main(){
  long squareOfTheSum = 0;
  long sumOfTheSquare = 0;
  long i = 0;
  for(i = 1; i <= 100; i++){
    sumOfTheSquare += i*i;
    squareOfTheSum += i;
  }
  squareOfTheSum = squareOfTheSum*squareOfTheSum;
  printf("%ld \n", squareOfTheSum-sumOfTheSquare);

  return 0;
}
