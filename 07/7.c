#include <math.h>
#include <stdio.h>

int is_prime(long long);

int main(){
  long long i = 2;
  long long prime = 0;
  long long num = 0;
  while(num != 10001){
    if (is_prime(i) == 1){
      num++;
      prime = i;
    }
    i++;
  }
  printf("%lld\n", prime);
  return 0;
}

int is_prime(long long num){
  long long j;
  int n_is_prime = 1;
  for(j = 2; j <= sqrt(num); j++){
    if(num % j == 0){
      n_is_prime = 0;
    }
  }
  return n_is_prime;

}
