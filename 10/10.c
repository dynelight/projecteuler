#include <math.h>
#include <stdio.h>

int is_prime(long long);

int main(){
  long long i;
  long long acc = 0;
  for(i = 2; i < 2000000; i++){
    if (is_prime(i) == 1){
      acc += i;
    }
  }
  printf("%lld\n", acc);
  return 0;
}

int is_prime(long long num){
  long long j;
  int n_is_prime = 1;
  for(j = 2; j <= sqrt(num); j++){
    if(num % j == 0){
      n_is_prime = 0;
    }
  }
  return n_is_prime;

}
