from decimal import *

def is_increasing(n):
    str_n = str(n)
    cont = True
    i, prev = 0, 0

    while cont and i < len(str_n):
        if int(str_n[i]) >= prev:
            prev = int(str_n[i])
            i += 1
        else:
            cont = False
    return cont

def is_decreasing(n):
    str_n = str(n)
    cont = True
    i, prev = 0, 10

    while cont and i < len(str_n):
        if int(str_n[i]) <= prev:
            prev = int(str_n[i])
            i += 1
        else:
            cont = False
    return cont

def is_bouncy(n):
    return is_increasing(n) == False and is_decreasing(n) == False

bouncy, n_bouncy = 0, 0


for i in range(100, 100+1, 1):
    #print i, Decimal(bouncy)/Decimal(i)
    if is_bouncy(int(i)):
        bouncy += 1
    else:
        n_bouncy += 1

print n_bouncy
