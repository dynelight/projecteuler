#include<stdio.h>
#include<math.h>

int main(){
  long num_factores = 0; 
  long i = 0;
  long j = 1; 
  long triangular = 0; 

  do {
    num_factores = 0; 
    for(j = 1; j <= sqrt(triangular); j++){
      if(triangular % j == 0){
        ++num_factores;
        ++num_factores;
      }
    }
    if(num_factores < 500){
      ++i;
      triangular += i;
    }
  } while(num_factores < 500);

  printf("%ld\n", triangular);
  return 0;

}
