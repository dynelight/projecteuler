#include <stdio.h>

int seqPar(int);
int seqImpar(int);

int main ()
{
  int n = 0;
  int acc = 0;
  int max = 0;
  int i = 0;

  for(i = 1; i < 1000000; i++){
    n = i;
    acc = 0;
    do {
      if(n % 2 == 0){
        n = seqImpar(n);
      }
      else {
        n = seqPar(n);
      }
      ++acc;
    } while (n != 1);
   if(acc > max){
     max = acc;
    }
  }
  printf("%d/n", max);

  return 0;
}


int seqPar(int num){
  return num/2;
}

int seqImpar(int num){
  return 3*num + 1;
}
