def fill_in_letter_vector():
    global number_of_letter
    # 1 to 9
    number_of_letter[1] = 3 #one
    number_of_letter[2] = 3 #two
    number_of_letter[3] = 5 #three
    number_of_letter[4] = 4 #four
    number_of_letter[5] = 4 #five
    number_of_letter[6] = 3 #six
    number_of_letter[7] = 5 #seven
    number_of_letter[8] = 5 #eight
    number_of_letter[9] = 4 #nine
    number_of_letter[10] = 3 #ten
    number_of_letter[11] = 6 #eleven
    number_of_letter[12] = 6 #twelve
    number_of_letter[13] = 8 #thirteen
    number_of_letter[14] = 8 #fourteen
    number_of_letter[15] = 7 #fifteen
    number_of_letter[16] = 7 #sixteen
    number_of_letter[17] = 9 #seventeen
    number_of_letter[18] = 8 #eighteen
    number_of_letter[19] = 8 #nineteen

    #10 to 90

    number_of_letter[10] = 3 #ten
    number_of_letter[20] = 6 #twenty
    number_of_letter[30] = 6 #thirty
    number_of_letter[40] = 6 #fourty
    number_of_letter[50] = 5 #fifty
    number_of_letter[60] = 5 #sixty
    number_of_letter[70] = 7 #seventy
    number_of_letter[80] = 6 #eighty
    number_of_letter[90] = 6 #ninety

    #100 to 900
    for hundreds in range(100, 1000, 100):
        number_of_letter[hundreds] = number_of_letter[hundreds/100] + 7 #prefix + hundred

    #1000
    number_of_letter[1000] = 11 #onethousand = 11 

    # 21-100 w/out 30,40...

    for number in range(21, 100):
        if number % 10 != 0:
            number_of_letter[number] = number_of_letter[10*(number/10)] + number_of_letter[number % 10]

    #101-999 w/out 200, 300...

    for number in range(101, 1000):
        if number % 100 != 0:
            number_of_letter[number] = number_of_letter[100*(number/100)] + number_of_letter[number % 100] + 3 #and

number_of_letter = []
tot_sum = 0

for i in range(0, 1000+1):
    number_of_letter.append(-1)

fill_in_letter_vector()
#for i in range(1, 5+1):
for i in range(1, 1000+1):
    print i, number_of_letter[i]
    tot_sum += number_of_letter[i]

print tot_sum
