def sum_proper_divisors(number):
    sum = 0
    for i in range(1, number):
        if number % i == 0:
            sum = sum + i
    return sum

sum_proper_divisors_lst = []
is_amicable = []

for i in range(0, 10000):
    sum_proper_divisors_lst.append(-1)
    is_amicable.append(False)

for i in range(1, 10000):
    for j in range(i + 1, 10000):
        if(sum_proper_divisors_lst[i] == -1):
            sum_proper_divisors_lst[i] = sum_proper_divisors(i)
        if(sum_proper_divisors_lst[j] == -1):
            sum_proper_divisors_lst[j] = sum_proper_divisors(j)
        if sum_proper_divisors_lst[i] == j and sum_proper_divisors_lst[j] == i:
            print "Pair: " + str(i) + "," + str(j)
            is_amicable[i] = True
            is_amicable[j] = True


sum = 0
for k in range(1, 10000):
    if is_amicable[k]:
        sum = sum + sum_proper_divisors_lst[k]

print sum
