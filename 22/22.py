total_score = 0

f = open("names.txt", mode="r")

lines = f.readline().replace("\"", "").split(",")
lines.sort()

f.close()

#print lines[937]

i = 1

for name in lines:
    score = 0
    for char in name:
        score += (ord(char) - 64)
    total_score += i* score
    i += 1

print total_score
