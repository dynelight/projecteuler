import math

primes = []
num_circular = 0
limit = 9999999

def eratosthenes(limit):
    global primes
    for i in range(0, limit + 1): 
        primes.append(True)
    primes[0] = True
    primes[1] = True
    for p in range(2, int(math.sqrt(limit)) + 1, 1):
        if primes[p] == True:
            for q in range(p**2, limit, p):
                primes[q] = False
        if p == 2: 
            p = p + 1

def num_consecutive_primes(a, b):
    cont = True
    n = 0
    while cont:
        if primes[(n*n + a*n + b)]:
            n = n+1
        else:
            cont = False
    return n

eratosthenes(limit)
max_consec_primes = 0
max_a = -1001
max_b = -1001

for a in range(-1000, 1000 + 1):
    for b in range(-1000, 1000 + 1):
        num_consec_primes = num_consecutive_primes(a, b)
        if num_consec_primes > max_consec_primes:
            print a, b
            max_a = a
            max_b = b
            max_consec_primes = num_consec_primes

print max_a*max_b

