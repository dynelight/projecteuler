#include<stdio.h>

int main(){
  int *set;
  int i;
  set = new int[10001];

  for(i = 0; i < 10001; i++){
    set[i] = 0;
  }
  int a = 0;
  int b = 0;

  int aMax = 100;
  int bMax = 100;

  int acc = 1;

  int res = 0;

  for(a = 2; a <= 100; a++){
   acc = 1;
    for(b = 2; b <= 100; b++){
      acc *= a;
    }
    set[acc] = 1;
  }

  for(i = 1; i <10001;i++){
    if(set[i])
      res++;
   }

  printf("%d\n", res);
  return 0;

}
