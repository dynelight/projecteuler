import math

primes = []
sum_trunc = 0
limit = 9999999

def eratosthenes(limit):
    global primes
    for i in range(0, limit + 1): 
        primes.append(True)
    primes[0] = True
    primes[1] = True
    for p in range(2, int(math.sqrt(limit)) + 1, 1):
        if primes[p] == True:
            for q in range(p**2, limit, p):
                primes[q] = False
        if p == 2: 
            p = p + 1

def is_truncable(number):
    orig_num_str = str(number)
    len_num = len(orig_num_str)
    cont = True
    it_len_num = 0

    left_num_str = orig_num_str
    right_num_str = orig_num_str

    if orig_num_str[0] == '1' or orig_num_str[-1] == '1':
        cont = False

    while cont and it_len_num != len_num:
        if primes[int(left_num_str)] == False:
            cont = False
        it_len_num += 1
        left_num_str = left_num_str[1:] 

    it_len_num = 0

    while cont and it_len_num != len_num:
        if primes[int(right_num_str)] == False:
            cont = False
        it_len_num += 1
        if it_len_num != len_num:
            right_num_str = right_num_str[:len(right_num_str) - 1] 
    return cont

eratosthenes(limit)

num = 0
for i in range(10, 9999999):
    if is_truncable(i):
        sum_trunc += i
        num += 1
        print num, i

print sum_trunc

