#include <stdio.h>

int isTriangular(int);

int main ()
{
  FILE * pFile;
  int c;
  int n = 0;
  int numTriangular = 0;
  int acc = 0;
  int act;
  pFile=fopen ("words.txt","r");
  if (pFile==NULL) perror ("Error opening file");
  else
  {
    do {
      c = fgetc (pFile);
      if (c == '"'){
       acc = 0;
        do {
          c = fgetc (pFile);
            if (c != '"'){
              acc += (c-64);
            }
         } while (c != '"');
         if(isTriangular(acc) == 1){
           ++numTriangular;
         }
      }
    } while (c != EOF);
    fclose (pFile);
    printf ("%d\n",numTriangular);
  }
  return 0;
}


int isTriangular(int num){
  int isTriangular = 0;
  int i;
  for(i = 1; i <= num; i++){
    if(num == 0.5*i*(i+1)){
      isTriangular = 1;
    }
  }
  return isTriangular;
}
