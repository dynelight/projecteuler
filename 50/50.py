import math

primes = []
num_circular = 0
limit = 9999999

def eratosthenes(limit):
    global primes
    for i in range(0, limit + 1): 
        primes.append(True)
    primes[0] = True
    primes[1] = True
    for p in range(2, int(math.sqrt(limit)) + 1, 1):
        if primes[p] == True:
            for q in range(p**2, limit, p):
                primes[q] = False
        if p == 2: 
            p = p + 1

def is_circular(number):
    num_str = str(number)
    len_num = len(num_str)
    rotate = True
    it_len_num = 0
    while rotate and it_len_num != len_num:
        num_str = num_str[1:] + num_str[0] 
        if primes[int(num_str)] == False:
            rotate = False
        it_len_num += 1
    return rotate

eratosthenes(limit)

for i in range(2, 1000000):
    if is_circular(i):
        num_circular += 1

print num_circular

