import math

def eratosthenes(limit):
    primes = []
    for i in range(0, limit + 1): 
        primes.append(True)
    primes[0] = True
    primes[1] = True
    for p in range(2, int(math.sqrt(limit)) + 1, 1):
        if primes[p] == True:
            for q in range(p**2, limit, p):
                primes[q] = False
        if p == 2: 
            p = p + 1
    return primes

#limit = 50
limit = 50000000
list_ans = []

print "Calculating primes... "
primes = eratosthenes(limit)
tot = 0
print "Calculating answers... "

for a in range(2, int(math.ceil(limit ** (1.0/2.0))) + 1):
    for b in range(2, int(math.ceil(limit ** (1.0/3.0))) + 1):
        for c in range(2, int(math.ceil(limit ** (1.0/4.0))) + 1):
            if primes[a] and primes[b] and primes[c]:
                ans = a**2 + b**3 + c**4
                if ans < limit:
                    list_ans.append(ans)


print len(list_ans)

print len(set(list_ans)) 
                
