max_ln = -1
max_i = -1
import math

def exp_by_squaring(x, n):
    if n == 1:
        return x
    elif n % 2 == 0:
        return exp_by_squaring(x*x, n/2)
    else:
        return x * exp_by_squaring(x*x, (n-1)/2)


total_score = 0

f = open("base_exp.txt", mode="r")

lines = f.readlines()
f.close()

for i in range(0, len(lines)):
    lines[i] = lines[i].replace("\n", "")

i = 1

for line in lines:
    print i
    vals = line.split(",")

    # Inefficient!!
    # tmp = int(vals[0]) ** int(vals[1])

    #More efficient
    tmp = int(vals[1]) * math.log(int(vals[0]))
    if tmp > max_ln:
        max_ln = tmp
        max_i = i
    i = i+1

print max_i




