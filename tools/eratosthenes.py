import math

def eratosthenes(limit):
    primes = []
    for i in range(0, limit + 1): 
        primes.append(True)
    primes[0] = True
    primes[1] = True
    for p in range(2, int(math.sqrt(limit)) + 1, 1):
        if primes[p] == True:
            for q in range(p**2, limit, p):
                primes[q] = False
        if p == 2: 
            p = p + 1
    return primes

limit = 100
primes = eratosthenes(limit)
for i in range(0,limit):
    print(str(i) + ': ' + str(primes[i]))
